#!/bin/sh

if [ $# -ne 2 ]; then
    printf "Usage: number of runs, results directory\n"
    exit
fi

benchmarks="best_effort cublas double_block general_block naive register"

maxruns="$1"
results="$2"
mkdir -p "$results"

make -j

# Warmup
bin/best_effort 8192 8192 8192 > /dev/null 2> /dev/null

run=1
while [ $run -le "$maxruns" ]
do
    printf "Run %d\n" "$run"
    for benchmark in $benchmarks
    do
        bin/"$benchmark" 8192 8192 8192 >> "$results/$benchmark.raw" 2> /dev/null
    done

    run=$(( run + 1 ))
done

for benchmark in $benchmarks
do
    {
        python3 stddev.py "${results}/$benchmark.raw"
    } > "$results"/"$benchmark".res
done

{
    echo "benchmark, compute rate (Gflops/s), stddev"
    for benchmark in $benchmarks
    do
        printf "%s, " "$benchmark"
        cat "$results"/"$benchmark".res
    done
} > "$results"/results.csv
