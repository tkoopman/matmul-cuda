#include <stdio.h>
#include <stdlib.h>
#include <cublas_v2.h>
#include <debug.h>

int ceil(int a, int b)
{
    return (a + b - 1) / b;
}

__global__
void init(float *d_x, int m, int n, float value)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    int j = blockIdx.y * blockDim.y + threadIdx.y;

    if ((i < m) && (j < n)) {
        d_x[i * n + j] = value;
    }
}

int main(int argc, char **argv)
{
    if (argc != 4) {
        printf("Usage: M K N\n");
        return EXIT_FAILURE;
    }

    int m = atoi(argv[1]);
    int k = atoi(argv[2]);
    int n = atoi(argv[3]);

    float *d_a, *d_b, *d_c;
    CUDA_SAFE(cudaMalloc(&d_a, m * k * sizeof(float)));
    CUDA_SAFE(cudaMalloc(&d_b, k * n * sizeof(float)));
    CUDA_SAFE(cudaMalloc(&d_c, m * n * sizeof(float)));

    dim3 threadsPerBlock(16, 16);
    dim3 blocks_a(ceil(m, 16), ceil(k, 16));
    dim3 blocks_b(ceil(k, 16), ceil(n, 16));
    dim3 blocks_c(ceil(m, 16), ceil(n, 16));
    init<<<blocks_a, threadsPerBlock>>>(d_a, m, k, 1.0);
    init<<<blocks_b, threadsPerBlock>>>(d_b, k, n, 1.0);
    init<<<blocks_c, threadsPerBlock>>>(d_c, m, n, 0.0);

    float *c = (float *)malloc(m * n * sizeof(float));

    cublasHandle_t handle;
    CUBLAS_SAFE(cublasCreate(&handle));

    float duration;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventRecord(start, 0);

    const float alpha = 1.0;
    const float beta = 0.0;
    CUBLAS_SAFE(cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k,
                            &alpha, d_a, m, d_b, k, &beta, d_c, m));

    cudaEventCreate(&stop);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&duration, start,stop);

    duration /= 1e3;

    CUDA_SAFE(cudaMemcpy(c, d_c, m * n * sizeof(float),
                cudaMemcpyDeviceToHost));

    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if (c[i * n + j] != k) {
                printf("c[%d, %d] = %f\n", i, j, c[i * n + j]);
                goto loop_break;
            }
        }
    }
loop_break:

    fprintf(stderr, "Gflops/s: ");
    printf("%f\n", 2.0 * m * n * k / duration / 1e9);

    CUBLAS_SAFE(cublasDestroy(handle));
    CUDA_SAFE(cudaFree(d_a));
    CUDA_SAFE(cudaFree(d_b));
    CUDA_SAFE(cudaFree(d_c));

    return EXIT_SUCCESS;
}
