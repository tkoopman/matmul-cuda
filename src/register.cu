/* Two level of blocking, this time general
 *
 * C[bbi, bbj] = sum_bbp matmul(A[bbi, bbp], B[bbp, bbj]).
 *
 * The outer level of blocking on bbi, bbj is done by CUDA's blocking
 * mechanism. The second level is done with loops, the final one implicit
 * in the thread ids. We use [8, 1] x [1, 8] on the inner-level to get
 * 8 fold-reuse out of the shared memory. We still want 16^2 = 256 threads
 * so that determines M2B, N2B, and then we take K2B as large as possible
 * without running out of shared memory. A larger K2B is advantageous
 * because it improves the flops / store of C.
 * We use a register file to cache parts of the last level blocking. */

#include <stdio.h>
#include <stdlib.h>
#include <cublas_v2.h>
#include <debug.h>

#define M1B 8
#define M2B 16

#define K1B 1
#define K2B 8

#define N1B 8
#define N2B 16

/* [m / (M2B * M1B), k / (K2B * K1B), M1B, K1B, M2B, K2B]
 * array. */
#define IDA(bbi, bbj, bi, bj, i, j) ((bbi) * M2B * M1B * k + \
                                     (bbj) * M1B * K1B * M2B * K2B + \
                                     (bi) * K1B * M2B * K2B + \
                                     (bj) * M2B * K2B + \
                                     (i) * K2B + \
                                     (j))
/* Same but k, n */
#define IDB(bbi, bbj, bi, bj, i, j) ((bbi) * K2B * K1B * n + \
                                     (bbj) * K1B * N1B * K2B * N2B + \
                                     (bi) * N1B * K2B * N2B + \
                                     (bj) * K2B * N2B + \
                                     (i) * N2B + \
                                     (j))
/* Same but m, n */
#define IDC(bbi, bbj, bi, bj, i, j) ((bbi) * M2B * M1B * n + \
                                     (bbj) * M1B * N1B * M2B * N2B + \
                                     (bi) * N1B * M2B * N2B + \
                                     (bj) * M2B * N2B + \
                                     (i) * N2B + \
                                     (j))

__host__ __device__
int ceil(int a, int b)
{
    return (a + b - 1) / b;
}

__global__
void init(float *d_x, int m, float value)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < m) {
        d_x[i] = value;
    }
}

__global__
void matmul(float *a, float *b, float *c, int m, int k, int n)
{
    /* Map the thread space 'the wrong way around' to the matrices
     * for coalescing. */
    int j = threadIdx.x;
    int i = threadIdx.y;
    int bbj = blockIdx.x;
    int bbi = blockIdx.y;

    float sum[M1B][N1B] = {0.0};
    __shared__ __align__(1024) float ashare[M1B][K1B][M2B][K2B];
    __shared__ __align__(1024) float bshare[K1B][N1B][K2B][N2B];

    for (int bbp = 0; bbp < k / (K1B * K2B); bbp++) {
        /* Load in A[bbi, bbp], B[bbp, bbj]. Using the unused threadid to
         * parallelise for p is not very general, but this may be solved
         * by flattening the thread-space in a code-generator and letting
         * that figure out the best way to parallelise loops like this. */
        for (int bi = 0; bi < M1B; bi++) {
            for (int bp = 0; bp < K1B; bp++) {
                #pragma unroll
                for (int p = threadIdx.x; p < K2B; p += blockDim.x) {
                    ashare[bi][bp][i][threadIdx.x] =
                        a[IDA(bbi, bbp, bi, bp, i, threadIdx.x)];
                }
            }
        }
        for (int bp = 0; bp < K1B; bp++) {
            for (int bj = 0; bj < N1B; bj++) {
                #pragma unroll
                for (int p = threadIdx.y; p < K2B; p += blockDim.y) {
                    bshare[bp][bj][threadIdx.y][j] =
                        b[IDB(bbp, bbj, bp, bj, threadIdx.y, j)];
                }
            }
        }
        __syncthreads();

        float a_register[M1B];
        float b_register[N1B];
        for (int bp = 0; bp < K1B; bp++) {
            for (int p = 0; p < K2B; p++) {
                /* ashare[-][bp][i][p] is reused N1B -> register-file. */
                for (int bi = 0; bi < M1B; bi++) {
                    a_register[bi] = ashare[bi][bp][i][p];
                }
                /* bshare[bp][-][p][j] is reused M1B -> register-file. */
                for (int bj = 0; bj < N1B; bj++) {
                    b_register[bj] = bshare[bp][bj][p][j];
                }
                for (int bi = 0; bi < M1B; bi++) {
                    for (int bj = 0; bj < N1B; bj++) {
                        sum[bi][bj] += a_register[bi] * b_register[bj];
                    }
                }
            }
        }

        __syncthreads();
    }

    for (int bi = 0; bi < M1B; bi++) {
        for (int bj = 0; bj < N1B; bj++) {
            c[IDC(bbi, bbj, bi, bj, i, j)] = sum[bi][bj];
        }
    }
}

int main(int argc, char **argv)
{
    if (argc != 4) {
        printf("Usage: M K N\n");
        return EXIT_FAILURE;
    }

    int m = atoi(argv[1]);
    int k = atoi(argv[2]);
    int n = atoi(argv[3]);

    float *d_a, *d_b, *d_c;
    CUDA_SAFE(cudaMalloc(&d_a, m * k * sizeof(float)));
    CUDA_SAFE(cudaMalloc(&d_b, k * n * sizeof(float)));
    CUDA_SAFE(cudaMalloc(&d_c, m * n * sizeof(float)));

    init<<<ceil(m * k, 256), 256>>>(d_a, m * k, 1.0);
    init<<<ceil(k * n, 256), 256>>>(d_b, k * n, 1.0);
    init<<<ceil(m * n, 256), 256>>>(d_c, m * n, 0.0);

    dim3 threadsPerBlock(N2B, M2B);
    dim3 blocks(n / (N2B * N1B), m / (M2B * M1B));

    float *c = (float *)malloc(m * n * sizeof(float));

    float duration;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventRecord(start, 0);

    matmul<<<blocks, threadsPerBlock>>>(d_a, d_b, d_c, m, k, n);

    cudaEventCreate(&stop);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&duration, start,stop);

    duration /= 1e3;

    CUDA_SAFE(cudaMemcpy(c, d_c, m * n * sizeof(float),
                cudaMemcpyDeviceToHost));

    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if (c[i * n + j] != k) {
                printf("c[%d, %d] = %f\n", i, j, c[i * n + j]);
                goto loop_break;
            }
        }
    }
loop_break:

    fprintf(stderr, "Gflops/s: ");
    printf("%f\n", 2.0 * m * n * k / duration / 1e9);

    CUDA_SAFE(cudaFree(d_a));
    CUDA_SAFE(cudaFree(d_b));
    CUDA_SAFE(cudaFree(d_c));
    free(c);

    return EXIT_SUCCESS;
}
