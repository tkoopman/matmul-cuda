/* My best optimisation effort, sacrificing generality of implementation for
 * raw performance.
 *
 * Two level of blocking, this time we allow the inner blocking to be
 * [B1, K1B] x [K1B, B1], but the outer is still square. We do this to simplify
 * the loop-logic in several spots.
 *
 * C[bbi, bbj] = sum_bbp matmul(A[bbi, bbp], B[bbp, bbj]).
 *
 * The outer level of blocking on bbi, bbj is done by CUDA's blocking
 * mechanism. The second level is done with loops, the final one implicit
 * in the thread ids. We use [8, 1] x [1, 8] on the inner-level to get
 * 8 fold-reuse out of the shared memory. */

#include <stdio.h>
#include <stdlib.h>
#include <cublas_v2.h>
#include <debug.h>

#define B1 8
#define K1B 1
#define B1 8

#define B2 16

/* [m / (B2 * B1), k / (B2 * K1B), B1, K1B, B2, B2]
 * array. */
#define IDA(bbi, bbj, bi, bj, i, j) ((bbi) * B2 * B1 * k + \
                                     (bbj) * B1 * K1B * B2 * B2 + \
                                     (bi) * K1B * B2 * B2 + \
                                     (bj) * B2 * B2 + \
                                     (i) * B2 + \
                                     (j))
/* Same but k, n */
#define IDB(bbi, bbj, bi, bj, i, j) ((bbi) * B2 * K1B * n + \
                                     (bbj) * K1B * B1 * B2 * B2 + \
                                     (bi) * B1 * B2 * B2 + \
                                     (bj) * B2 * B2 + \
                                     (i) * B2 + \
                                     (j))
/* Same but m, n */
#define IDC(bbi, bbj, bi, bj, i, j) ((bbi) * B2 * B1 * n + \
                                     (bbj) * B1 * B1 * B2 * B2 + \
                                     (bi) * B1 * B2 * B2 + \
                                     (bj) * B2 * B2 + \
                                     (i) * B2 + \
                                     (j))

__host__ __device__
int ceil(int a, int b)
{
    return (a + b - 1) / b;
}

__global__
void init(float *d_x, int m, float value)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < m) {
        d_x[i] = value;
    }
}

__global__
void matmul(float *a, float *b, float *c, int m, int k, int n)
{
    /* Map the thread space 'the wrong way around' to the matrices
     * for coalescing. */
    int j = threadIdx.x;
    int i = threadIdx.y;
    int bbj = blockIdx.x;
    int bbi = blockIdx.y;

    float sum[B1][B1] = {0.0};
    __shared__ __align__(1024) float ashare[B1][K1B][B2][B2];
    __shared__ __align__(1024) float bshare[K1B][B1][B2][B2];

    for (int bbp = 0; bbp < k / (K1B * B2); bbp++) {
        /* Load in A[bbi, bbp], B[bbp, bbj]. */
        for (int bx = 0; bx < B1; bx++) {
            for (int bp = 0; bp < K1B; bp++) {
                    ashare[bx][bp][i][j] =
                        a[IDA(bbi, bbp, bx, bp, i, j)];
                    bshare[bp][bx][i][j] =
                        b[IDB(bbp, bbj, bp, bx, i, j)];
            }
        }
        __syncthreads();

        for (int bp = 0; bp < K1B; bp++) {
            for (int p = 0; p < B2; p++) {
                for (int bi = 0; bi < B1; bi++) {
                    for (int bj = 0; bj < B1; bj++) {
                        sum[bi][bj] += ashare[bi][bp][i][p] *
                                       bshare[bp][bj][p][j];
                    }
                }
            }
        }

        __syncthreads();
    }

    for (int bi = 0; bi < B1; bi++) {
        for (int bj = 0; bj < B1; bj++) {
            c[IDC(bbi, bbj, bi, bj, i, j)] = sum[bi][bj];
        }
    }
}

int main(int argc, char **argv)
{
    if (argc != 4) {
        printf("Usage: M K N\n");
        return EXIT_FAILURE;
    }

    int m = atoi(argv[1]);
    int k = atoi(argv[2]);
    int n = atoi(argv[3]);

    float *d_a, *d_b, *d_c;
    CUDA_SAFE(cudaMalloc(&d_a, m * k * sizeof(float)));
    CUDA_SAFE(cudaMalloc(&d_b, k * n * sizeof(float)));
    CUDA_SAFE(cudaMalloc(&d_c, m * n * sizeof(float)));

    init<<<ceil(m * k, 256), 256>>>(d_a, m * k, 1.0);
    init<<<ceil(k * n, 256), 256>>>(d_b, k * n, 1.0);
    init<<<ceil(m * n, 256), 256>>>(d_c, m * n, 0.0);

    dim3 threadsPerBlock(B2, B2);
    dim3 blocks(n / (B2 * B1), m / (B2 * B1));

    float *c = (float *)malloc(m * n * sizeof(float));

    float duration;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventRecord(start, 0);

    matmul<<<blocks, threadsPerBlock>>>(d_a, d_b, d_c, m, k, n);

    cudaEventCreate(&stop);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&duration, start,stop);

    duration /= 1e3;

    CUDA_SAFE(cudaMemcpy(c, d_c, m * n * sizeof(float),
                cudaMemcpyDeviceToHost));

    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if (c[i * n + j] != k) {
                printf("c[%d, %d] = %f\n", i, j, c[i * n + j]);
                goto loop_break;
            }
        }
    }
loop_break:

    fprintf(stderr, "Gflops/s ");
    printf("%f\n", 2.0 * m * n * k / duration / 1e9);

    CUDA_SAFE(cudaFree(d_a));
    CUDA_SAFE(cudaFree(d_b));
    CUDA_SAFE(cudaFree(d_c));
    free(c);

    return EXIT_SUCCESS;
}
