#include <stdio.h>
#include <stdlib.h>
#include <cublas_v2.h>
#include <debug.h>

#define ITER 20

void init(float *x, int m, int n, float value)
{
    for (int i = 0; i < m * n; i++) {
        x[i] = value;
    }
}

int main(int argc, char **argv)
{
    if (argc != 4) {
        printf("Usage: M K N\n");
        return EXIT_FAILURE;
    }

    int m = atoi(argv[1]);
    int k = atoi(argv[2]);
    int n = atoi(argv[3]);

    float *a, *b, *c;
    CUDA_SAFE(cudaMallocManaged(&a, m * k * sizeof(float)));
    CUDA_SAFE(cudaMallocManaged(&b, k * n * sizeof(float)));
    CUDA_SAFE(cudaMallocManaged(&c, m * n * sizeof(float)));

    init(a, m, k, 1.0);
    init(b, k, n, 1.0);
    init(c, m, n, 0.0);

    cublasHandle_t handle;
    CUBLAS_SAFE(cublasCreate(&handle));

    float duration;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventRecord(start, 0);

    const float alpha = 1.0;
    const float beta = 0.0;

    /* In contrast to the unmanaged version, we also time the transfers, 
       amortize it by running the computation may time. */
    for (int t = 0; t < ITER; t++) {
        CUBLAS_SAFE(cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k,
                                &alpha, a, m, b, k, &beta, c, m));
    }

    cudaEventCreate(&stop);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&duration, start,stop);

    duration /= 1e3;

    fprintf(stderr, "Gflops/s: ");
    printf("%f\n", 2.0 * m * n * k * ITER / duration / 1e9);

    CUBLAS_SAFE(cublasDestroy(handle));
    CUDA_SAFE(cudaFree(a));
    CUDA_SAFE(cudaFree(b));
    CUDA_SAFE(cudaFree(c));

    return EXIT_SUCCESS;
}
