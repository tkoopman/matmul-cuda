/* We transpose the index space, not the matrices. */

#include <stdio.h>
#include <stdlib.h>
#include <cublas_v2.h>
#include <debug.h>

int ceil(int a, int b)
{
    return (a + b - 1) / b;
}

__global__
void init(float *d_x, int m, int n, float value)
{
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    int j = blockIdx.x * blockDim.x + threadIdx.x;

    if ((i < m) && (j < n)) {
        d_x[i * n + j] = value;
    }
}

__global__
void matmul(float *d_a, float *d_b, float *d_c, int m, int k, int n)
{
    int i = blockIdx.y * blockDim.y + threadIdx.y;
    int j = blockIdx.x * blockDim.x + threadIdx.x;

    float temp = 0.0;
    if ((i < m) && (j < n)) {
        for (int p = 0; p < k; p++) {
            temp += d_a[i * k + p] * d_b[p * n + j];
        }
        d_c[i * n + j] = temp;
    }
}

int main(int argc, char **argv)
{
    if (argc != 4) {
        printf("Usage: M K N\n");
        return EXIT_FAILURE;
    }

    int m = atoi(argv[1]);
    int k = atoi(argv[2]);
    int n = atoi(argv[3]);

    float *d_a, *d_b, *d_c;
    CUDA_SAFE(cudaMalloc(&d_a, m * k * sizeof(float)));
    CUDA_SAFE(cudaMalloc(&d_b, k * n * sizeof(float)));
    CUDA_SAFE(cudaMalloc(&d_c, m * n * sizeof(float)));

    dim3 threadsPerBlock(16, 16);
    dim3 blocks_a(ceil(k, 16), ceil(m, 16));
    dim3 blocks_b(ceil(n, 16), ceil(k, 16));
    dim3 blocks_c(ceil(n, 16), ceil(m, 16));
    init<<<blocks_a, threadsPerBlock>>>(d_a, m, k, 1.0);
    init<<<blocks_b, threadsPerBlock>>>(d_b, k, n, 1.0);
    init<<<blocks_c, threadsPerBlock>>>(d_c, m, n, 0.0);

    float *c = (float *)malloc(m * n * sizeof(float));

    float duration;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventRecord(start, 0);

    matmul<<<blocks_c, threadsPerBlock>>>(d_a, d_b, d_c, m, k, n);
    CUDA_SAFE(cudaDeviceSynchronize());

    cudaEventCreate(&stop);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&duration, start,stop);

    duration /= 1e3;

    CUDA_SAFE(cudaMemcpy(c, d_c, m * n * sizeof(float),
                cudaMemcpyDeviceToHost));

    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if (c[i * n + j] != k) {
                printf("c[%d, %d] = %f\n", i, j, c[i * n + j]);
                goto loop_break;
            }
        }
    }
loop_break:

    fprintf(stderr, "Gflops/s: ");
    printf("%f\n", 2.0 * m * n * k / duration / 1e9);

    CUDA_SAFE(cudaFree(d_a));
    CUDA_SAFE(cudaFree(d_b));
    CUDA_SAFE(cudaFree(d_c));

    return EXIT_SUCCESS;
}
