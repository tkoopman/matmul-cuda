/* Two level of blocking, one of [B1, B1] and one of [B2, B2].
 *
 * C[bbi, bbj] = sum_bbp matmul(A[bbi, bbp], B[bbp, bbj]).
 *
 * The outer level of blocking on bbi, bbj is done by CUDA's blocking
 * mechanism. The second level is done with loops, the final one implicit
 * in the thread ids. */

#include <stdio.h>
#include <stdlib.h>
#include <cublas_v2.h>
#include <debug.h>

#define B1 8
#define B2 8

/* [m / (B2 * B1), k / (B2 * B1), B1, B1, B2, B2]
 * array. */
#define IDA(bbi, bbj, bi, bj, i, j) ((bbi) * B2 * B1 * k + \
                                     (bbj) * B2 * B2 * B1 * B1 + \
                                     (bi) * B2 * B2 * B1 + \
                                     (bj) * B2 * B2 + \
                                     (i) * B2 + \
                                     (j))
/* Same but k, n */
#define IDB(bbi, bbj, bi, bj, i, j) ((bbi) * B2 * B1 * n + \
                                     (bbj) * B2 * B2 * B1 * B1 + \
                                     (bi) * B2 * B2 * B1 + \
                                     (bj) * B2 * B2 + \
                                     (i) * B2 + \
                                     (j))
/* Same but m, n */
#define IDC(bbi, bbj, bi, bj, i, j) IDB(bbi, bbj, bi, bj, i, j)

__host__ __device__
int ceil(int a, int b)
{
    return (a + b - 1) / b;
}

__global__
void init(float *d_x, int m, float value)
{
    int i = blockIdx.x * blockDim.x + threadIdx.x;

    if (i < m) {
        d_x[i] = value;
    }
}

__global__
void matmul(float *a, float *b, float *c, int m, int k, int n)
{
    /* Map the thread space 'the wrong way around' to the matrices
     * for coalescing. */
    int j = threadIdx.x;
    int i = threadIdx.y;
    int bbj = blockIdx.x;
    int bbi = blockIdx.y;

    float sum[B1][B1] = {0.0};
    __shared__ __align__(1024) float ashare[B1][B1][B2][B2];
    __shared__ __align__(1024) float bshare[B1][B1][B2][B2];

    for (int bbp = 0; bbp < k / (B1 * B2); bbp++) {
        /* Load in A[bbi, bbp], B[bbp, bbj]. */
        for (int bi = 0; bi < B1; bi++) {
            for (int bj = 0; bj < B1; bj++) {
                ashare[bi][bj][i][j] =
                    a[IDA(bbi, bbp, bi, bj, i, j)];
                bshare[bi][bj][i][j] =
                    b[IDB(bbp, bbj, bi, bj, i, j)];
            }
        }

        __syncthreads();

        for (int bp = 0; bp < B1; bp++) {
            for (int p = 0; p < B2; p++) {
                for (int bi = 0; bi < B1; bi++) {
                    for (int bj = 0; bj < B1; bj++) {
                        sum[bi][bj] += ashare[bi][bp][i][p] *
                                       bshare[bp][bj][p][j];
                    }
                }
            }
        }

        __syncthreads();
    }

    for (int bi = 0; bi < B1; bi++) {
        for (int bj = 0; bj < B1; bj++) {
            c[IDC(bbi, bbj, bi, bj, i, j)] = sum[bi][bj];
        }
    }
}

int main(int argc, char **argv)
{
    if (argc != 4) {
        printf("Usage: M K N\n");
        return EXIT_FAILURE;
    }

    int m = atoi(argv[1]);
    int k = atoi(argv[2]);
    int n = atoi(argv[3]);

    float *d_a, *d_b, *d_c;
    CUDA_SAFE(cudaMalloc(&d_a, m * k * sizeof(float)));
    CUDA_SAFE(cudaMalloc(&d_b, k * n * sizeof(float)));
    CUDA_SAFE(cudaMalloc(&d_c, m * n * sizeof(float)));

    init<<<ceil(m * k, 256), 256>>>(d_a, m * k, 1.0);
    init<<<ceil(k * n, 256), 256>>>(d_b, k * n, 1.0);
    init<<<ceil(m * n, 256), 256>>>(d_c, m * n, 0.0);

    dim3 threadsPerBlock(B2, B2);
    dim3 blocks(n / (B2 * B1), m / (B2 * B1));

    float *c = (float *)malloc(m * n * sizeof(float));

    float duration;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventRecord(start, 0);

    matmul<<<blocks, threadsPerBlock>>>(d_a, d_b, d_c, m, k, n);

    cudaEventCreate(&stop);
    cudaEventRecord(stop, 0);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&duration, start,stop);

    duration /= 1e3;

    CUDA_SAFE(cudaMemcpy(c, d_c, m * n * sizeof(float),
                cudaMemcpyDeviceToHost));

    for (int i = 0; i < m; i++) {
        for (int j = 0; j < n; j++) {
            if (c[i * n + j] != k) {
                printf("c[%d, %d] = %f\n", i, j, c[i * n + j]);
                goto loop_break;
            }
        }
    }
loop_break:

    fprintf(stderr, "Gflops/s: ");
    printf("%f\n", 2.0 * m * n * k / duration / 1e9);

    CUDA_SAFE(cudaFree(d_a));
    CUDA_SAFE(cudaFree(d_b));
    CUDA_SAFE(cudaFree(d_c));
    free(c);

    return EXIT_SUCCESS;
}
