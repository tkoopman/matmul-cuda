import statistics
import sys

data = []
inputfile = sys.argv[1]
with open(inputfile, "r") as f:
    for line in f.readlines():
        data.append(float(line))

stdev = 0
if len(data) > 1:
    stdev = statistics.stdev(data)
print(str(statistics.mean(data)) + ", " + str(stdev))
