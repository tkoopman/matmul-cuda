C = nvcc
FLAGS =
LFLAGS = -lm -lcublas
SRCS = $(wildcard src/*.cu)
BINS = $(patsubst src/%.cu, bin/%, $(SRCS))

all: $(BINS)

bin/%: src/%.cu
	$(C) -Iinclude $(FLAGS) $< -o $@ $(LFLAGS)

clean:
	$(RM) bin/*
